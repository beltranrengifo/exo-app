import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store.js'
import '@/assets/scss/main.scss'
import { formatDate, formatCurrency, capitalize, safeCSSClass } from './utils/filters'

Vue.config.productionTip = false

Vue.filter('formatDate', formatDate)
Vue.filter('formatCurrency', formatCurrency)
Vue.filter('capitalize', capitalize)
Vue.filter('safeCSSClass', safeCSSClass)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
