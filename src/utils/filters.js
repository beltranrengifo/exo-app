const formatDate = (value, locale, options, separator) => {
  if (value) {
    if (!(value instanceof Date)) {
      value = new Date(value)
    }
    if (separator) return value.toLocaleDateString(locale, options).replace(' ', separator + ' ')
    else return value.toLocaleDateString(locale, options)
  }
}

const formatCurrency = (value, locale, options) => {
  if (value) {
    return value.toLocaleString(locale, options)
  }
}

const capitalize = value => value.charAt(0).toUpperCase() + value.slice(1).toLowerCase()

const safeCSSClass = value => encodeURIComponent(value.toLowerCase()).replace(/%[0-9A-F]{2}/gi, '')

export {
  formatDate,
  formatCurrency,
  capitalize,
  safeCSSClass
}
