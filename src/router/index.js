import Vue from 'vue'
import Router from 'vue-router'
import UserPortfolio from '@/views/UserPortfolio'
import UserProfile from '@/views/UserProfile'
import UserTransfers from '@/views/UserTransfers'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'UserPortfolio',
      component: UserPortfolio
    },
    {
      path: '/profile',
      name: 'UserProfile',
      component: UserProfile
    },
    {
      path: '/transfers',
      name: 'UserTransfers',
      component: UserTransfers
    }
  ]
})
