import axios from 'axios'

const state = {
  userData: {}
}

const getters = {
  userName () {
    return state.userData.last_name
  },

  lastLogin () {
    return state.userData.last_login
  }

}

const actions = {
  async fetchUserData ({ commit }) {
    try {
      const endPoint = 'user'
      let userData = await axios.get(process.env.API_URL + endPoint)
      commit('SET_USER_DATA', userData.data)
      return userData
    } catch (error) {
      console.log(error)
    }
  }
}

const mutations = {

  'SET_USER_DATA' (state, payload) {
    state.userData = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
