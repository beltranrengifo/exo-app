import scssVars from '@/assets/scss/partials/_variables.scss'

const state = {
  phoneResolution: window.matchMedia(`(max-width: ${scssVars.isPhone})`).matches,
  loading: false
}

const actions = {
  checkResize ({ commit }) {
    commit('SET_PHONE_RES', window.matchMedia(`(max-width: ${scssVars.isPhone})`).matches)
  },

  loadingAction ({ commit }, payload) {
    commit('SET_LOADING', payload)
  }
}

const mutations = {
  'SET_PHONE_RES' (state, payload) {
    state.phoneResolution = payload
  },

  'SET_LOADING' (state, payload) {
    state.loading = payload
  }
}

export default {
  state,
  actions,
  mutations
}
