import axios from 'axios'
import _ from 'lodash'

const state = {
  userPortfolios: {},
  transfers: []
}

const getters = {
  getPortfolioFeatured () {
    if (!state.userPortfolios.latest_allocation) return
    const data = state.userPortfolios.latest_allocation
    const images = ['balance']
    for (const element in data) {
      images.forEach(el => {
        if (el === element) {
          data[el] = {
            value: data[el],
            image: require(`@/assets/images/image-${el}.png`)
          }
        }
      })
      if (element === 'earnings') {
        data[element] = {
          value: data[element],
          hasArrow: true
        }
      }
    }
    return _(data).toPairs().sortBy(0).fromPairs().value()
  },

  getPortfolioCurrency () {
    if (!state.userPortfolios.currency) return
    return state.userPortfolios.currency
  },

  getPortfolioMain () {
    if (Object.keys(state.userPortfolios).length > 0) {
      return state.userPortfolios
    }
  },

  getPortfolioTransfers () {
    return state.transfers ? state.transfers : []
  }
}

const actions = {

  async fetchPortfolioData ({ commit }) {
    try {
      const endPoint = 'portfolios'
      let userPortfolios = await axios.get(process.env.API_URL + endPoint)
      commit('SET_USER_PORTFOLIOS', userPortfolios.data.results[0])
      return userPortfolios.data.results[0]
    } catch (error) {
      console.log(error)
    }
  },

  async fetchPortfolioTransfers ({ commit }) {
    try {
      const endPoint = 'transfers'
      let transfers = await axios.get(process.env.API_URL + endPoint)
      commit('SET_USER_TRANSFERS', transfers.data.results)
      return transfers.data.results
    } catch (error) {
      console.log(error)
    }
  }
}

const mutations = {
  'SET_USER_PORTFOLIOS' (state, payload) {
    state.userPortfolios = payload
  },

  'SET_USER_TRANSFERS' (state, payload) {
    state.transfers = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
