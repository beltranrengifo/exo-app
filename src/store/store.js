import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import portfolio from './modules/portfolio'
import shared from './modules/shared'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    portfolio,
    shared
  }
})
